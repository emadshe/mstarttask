package com.emadshehadah.mstart.addeditevent

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.emadshehadah.mstart.data.Event
import com.emadshehadah.mstart.data.EventDao
import com.emadshehadah.mstart.home.ADD_EVENT_RESULT_OK
import com.emadshehadah.mstart.home.EDIT_EVENT_RESULT_OK
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch

class AddEditEventViewModel @ViewModelInject constructor(
    private val eventDao: EventDao,
    @Assisted private val state: SavedStateHandle
) : ViewModel() {

    val event = state.get<Event>("event")

    var eventName = state.get<String>("eventName") ?: event?.name ?: ""
        set(value) {
            field = value
            state.set("eventName", value)
        }

    var eventImportance = state.get<Boolean>("eventImportance") ?: event?.important ?: false
        set(value) {
            field = value
            state.set("eventImportance", value)
        }

    private val addEditEventEventChannel = Channel<AddEditEventEvent>()
    val addEditEventEvent = addEditEventEventChannel.receiveAsFlow()

    fun onSaveClick(date: String) {
        if (eventName.isBlank()) {
            showInvalidInputMessage("Name cannot be empty")
            return
        }

        if (event != null) {
            val updatedEvent = event.copy(name = eventName, important = eventImportance)
            updateEvent(updatedEvent)
        } else {
            val newEvent = Event(name = eventName, important = eventImportance, created = date)
            createEvent(newEvent)
        }
    }

    private fun createEvent(event: Event) = viewModelScope.launch {
        eventDao.insert(event)
        addEditEventEventChannel.send(AddEditEventEvent.NavigateBackWithResult(ADD_EVENT_RESULT_OK))
    }

    private fun updateEvent(event: Event) = viewModelScope.launch {
        eventDao.update(event)
        addEditEventEventChannel.send(AddEditEventEvent.NavigateBackWithResult(EDIT_EVENT_RESULT_OK))
    }

    private fun showInvalidInputMessage(text: String) = viewModelScope.launch {
        addEditEventEventChannel.send(AddEditEventEvent.ShowInvalidInputMessage(text))
    }

    sealed class AddEditEventEvent {
        data class ShowInvalidInputMessage(val msg: String) : AddEditEventEvent()
        data class NavigateBackWithResult(val result: Int) : AddEditEventEvent()
    }
}