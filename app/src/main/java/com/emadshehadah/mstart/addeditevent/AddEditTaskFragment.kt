package com.emadshehadah.mstart.addeditevent

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.emadshehadah.mstart.R
import com.emadshehadah.mstart.databinding.DialogAddEditBinding
 import com.emadshehadah.mstart.util.exhaustive
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class AddEditEventFragment : DialogFragment(R.layout.dialog_add_edit) {

    private val viewModel: AddEditEventViewModel by viewModels()
    private val args: AddEditEventFragmentArgs by navArgs()

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = DialogAddEditBinding.bind(view)

        binding.apply {
            etEventName.setText(viewModel.eventName)
            cbImportant.isChecked = viewModel.eventImportance
            cbImportant.jumpDrawablesToCurrentState()
            tvDateCreated.isVisible = viewModel.event != null
            tvDateCreated.text = "Created: ${viewModel.event?.created}"

            etEventName.doAfterTextChanged {
                viewModel.eventName = it.toString()
            }

            cbImportant.setOnCheckedChangeListener { _, isChecked ->
                viewModel.eventImportance = isChecked
            }

            btnSave.setOnClickListener {
                viewModel.onSaveClick(args.date)
            }
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.addEditEventEvent.collect { event ->
                when (event) {
                    is AddEditEventViewModel.AddEditEventEvent.ShowInvalidInputMessage -> {
                        Snackbar.make(requireView(), event.msg, Snackbar.LENGTH_LONG).show()
                    }
                    is AddEditEventViewModel.AddEditEventEvent.NavigateBackWithResult -> {
                        binding.etEventName.clearFocus()
                        setFragmentResult(
                            "add_edit_request",
                            bundleOf("add_edit_result" to event.result)
                        )
                        dismiss()
                    }
                }.exhaustive
            }
        }
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }
}