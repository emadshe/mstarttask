package com.emadshehadah.mstart.di

import android.app.Application
import androidx.room.Room
import com.emadshehadah.mstart.BuildConfig
import com.emadshehadah.mstart.data.EventDatabase
import com.emadshehadah.mstart.network.DateApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideDatabase(
        app: Application,
        callback: EventDatabase.Callback
    ) = Room.databaseBuilder(app, EventDatabase::class.java, "event_database")
        .fallbackToDestructiveMigration()
        .addCallback(callback)
        .build()

    @Provides
    fun provideEventDao(db: EventDatabase) = db.eventDao()

    @ApplicationScope
    @Provides
    @Singleton
    fun provideApplicationScope() = CoroutineScope(SupervisorJob())

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit =
        Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    @Provides
    @Singleton
    fun provideDateApi(retrofit: Retrofit): DateApi =
        retrofit.create(DateApi::class.java)

}

@Retention(AnnotationRetention.RUNTIME)
@Qualifier
annotation class ApplicationScope