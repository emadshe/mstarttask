package com.emadshehadah.mstart.convertdate

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.emadshehadah.mstart.R
import com.emadshehadah.mstart.databinding.FragmentDateConvertBinding
import com.emadshehadah.mstart.home.ADD_EVENT_RESULT_OK
import com.emadshehadah.mstart.network.Resource
  import com.emadshehadah.mstart.util.formatToDefaults
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class ConvertDateFragment : Fragment(R.layout.fragment_date_convert) {

    private val viewModel: ConvertDateViewModel by viewModels()

    private var selectedDate = ""
    private var formattedDate = ""

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val binding = FragmentDateConvertBinding.bind(view)

        binding.apply {
            tvDate.text = Calendar.getInstance().time.formatToDefaults()
            btnConvert.setOnClickListener {
                viewModel.doConvertDate(selectedDate)
            }
            btnSave.setOnClickListener {
                findNavController().navigate(
                    ConvertDateFragmentDirections.actionNavDateToAddEditEventFragment(
                        formattedDate,
                        null
                    )
                )

            }
        }

        viewModel.convertDateResult.observe(viewLifecycleOwner) { result ->
            when (result) {
                is Resource.Success -> {
                    formattedDate = result.value.data?.hijri?.date
                        ?: Calendar.getInstance().time.formatToDefaults()
                    binding.tvConvertedDate.text =
                        getString(R.string.converted_date) + " - " + result.value.data?.hijri?.date
                    binding.btnSave.isVisible = true
                }
                is Resource.Failure -> {
                    Toast.makeText(
                        requireContext(),
                        result.errorMessage,
                        Toast.LENGTH_LONG
                    ).show()
                    binding.btnSave.isVisible = false
                }
            }
        }
        binding.tvDate.setOnClickListener {
            showDateDialog()
        }

        setFragmentResultListener("add_edit_request") { _, bundle ->
            val result = bundle.getInt("add_edit_result")
            if (ADD_EVENT_RESULT_OK == result)
                Snackbar.make(
                    requireView(),
                    getString(R.string.event_has_been_added),
                    Snackbar.LENGTH_SHORT
                ).show()
        }
    }

    private fun showDateDialog() {
        val newCalendar = Calendar.getInstance()
        val newDate = Calendar.getInstance()

        val mDateFromPicker = DatePickerDialog(
            requireContext(), { _, year, monthOfYear, dayOfMonth ->
                newDate[year, monthOfYear] = dayOfMonth

                selectedDate = newDate.time.formatToDefaults()

            },
            newCalendar[Calendar.YEAR],
            newCalendar[Calendar.MONTH],
            newCalendar[Calendar.DAY_OF_MONTH]
        )
        mDateFromPicker.datePicker.maxDate = Date().time
        mDateFromPicker.show()

    }
}