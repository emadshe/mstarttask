package com.emadshehadah.mstart.convertdate

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.emadshehadah.mstart.data.BaseWrapper
import com.emadshehadah.mstart.convertdate.repository.DateRepository
import com.emadshehadah.mstart.data.DateResponse
import com.emadshehadah.mstart.network.Resource
import kotlinx.coroutines.launch

class ConvertDateViewModel @ViewModelInject constructor(
    private val dateRepository: DateRepository
) : ViewModel() {

    var convertDateResult = MutableLiveData<Resource<BaseWrapper<DateResponse>>>()
    fun doConvertDate(date: String) = viewModelScope.launch {
        convertDateResult.value = dateRepository.convertDate(date)
        convertDateResult.value = Resource.Loading
    }


}