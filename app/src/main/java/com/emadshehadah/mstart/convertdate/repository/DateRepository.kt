package com.emadshehadah.mstart.convertdate.repository

import com.emadshehadah.mstart.network.DateApi
import javax.inject.Inject

class DateRepository @Inject constructor(private val dateApi: DateApi) : BaseRepository() {
    suspend fun convertDate(date: String) = safeApiCall { dateApi.convertDate(date) }


}