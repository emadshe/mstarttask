package com.emadshehadah.mstart.util

import androidx.appcompat.widget.SearchView
import java.text.SimpleDateFormat
import java.util.*

inline fun SearchView.onQueryTextChanged(crossinline listener: (String) -> Unit) {
    this.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String?): Boolean {
            return true
        }

        override fun onQueryTextChange(newText: String?): Boolean {
            listener(newText.orEmpty())
            return true
        }
    })


}

fun Date.formatToDefaults(): String {
    val sdf = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
    return sdf.format(this)
}