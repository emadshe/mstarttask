package com.emadshehadah.mstart.util

val <T> T.exhaustive: T
    get() = this