package com.emadshehadah.mstart.events

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.emadshehadah.mstart.data.Event
import com.emadshehadah.mstart.databinding.RowEventBinding

class EventsAdapter(private val listener: OnItemClickListener) :
    ListAdapter<Event, EventsAdapter.EventsViewHolder>(DiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventsViewHolder {
        val binding = RowEventBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return EventsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: EventsViewHolder, position: Int) {
        val currentItem = getItem(position)
        holder.bind(currentItem)
    }

    inner class EventsViewHolder(private val binding: RowEventBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.apply {
                root.setOnClickListener {
                    val position = adapterPosition
                    if (position != RecyclerView.NO_POSITION) {
                        val event = getItem(position)
                        listener.onItemClick(event)
                    }
                }
                cbCompleted.setOnClickListener {
                    val position = adapterPosition
                    if (position != RecyclerView.NO_POSITION) {
                        val event = getItem(position)
                        listener.onCheckBoxClick(event, cbCompleted.isChecked)
                    }
                }
            }
        }

        fun bind(event: Event) {
            binding.apply {
                cbCompleted.isChecked = event.completed
                tvName.text = event.name
                tvName.paint.isStrikeThruText = event.completed
                ivImportant.isVisible = event.important
                tvDate.text = event.created
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(event: Event)
        fun onCheckBoxClick(event: Event, isChecked: Boolean)
    }

    class DiffCallback : DiffUtil.ItemCallback<Event>() {
        override fun areItemsTheSame(oldItem: Event, newItem: Event) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Event, newItem: Event) =
            oldItem == newItem
    }
}