package com.emadshehadah.mstart.events

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.emadshehadah.mstart.data.Event
import com.emadshehadah.mstart.data.EventDao
import com.emadshehadah.mstart.data.PreferencesManager
import com.emadshehadah.mstart.data.SortOrder
import com.emadshehadah.mstart.home.EDIT_EVENT_RESULT_OK
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch

class EventsViewModel @ViewModelInject constructor(
    private val eventDao: EventDao,
    private val preferencesManager: PreferencesManager,
    @Assisted private val state: SavedStateHandle
) : ViewModel() {

    val searchQuery = state.getLiveData("searchQuery", "")

    val preferencesFlow = preferencesManager.preferencesFlow

    private val eventsEventChannel = Channel<EventsEvent>()
    val eventsEvent = eventsEventChannel.receiveAsFlow()

    private val eventsFlow = combine(
        searchQuery.asFlow(),
        preferencesFlow
    ) { query, filterPreferences ->
        Pair(query, filterPreferences)
    }.flatMapLatest { (query, filterPreferences) ->
        eventDao.getEvents(query, filterPreferences.sortOrder, filterPreferences.hideCompleted)
    }

    val events = eventsFlow.asLiveData()

    fun onSortOrderSelected(sortOrder: SortOrder) = viewModelScope.launch {
        preferencesManager.updateSortOrder(sortOrder)
    }

    fun onHideCompletedClick(hideCompleted: Boolean) = viewModelScope.launch {
        preferencesManager.updateHideCompleted(hideCompleted)
    }

    fun onEventSelected(event: Event) = viewModelScope.launch {
        eventsEventChannel.send(EventsEvent.NavigateToEditEventScreen(event))
    }

    fun onEventCheckedChanged(event: Event, isChecked: Boolean) = viewModelScope.launch {
        eventDao.update(event.copy(completed = isChecked))
    }

    fun onEventSwiped(event: Event) = viewModelScope.launch {
        eventDao.delete(event)
        eventsEventChannel.send(EventsEvent.ShowUndoDeleteEventMessage(event))
    }

    fun onUndoDeleteClick(event: Event) = viewModelScope.launch {
        eventDao.insert(event)
    }


    fun onEditResult(result: Int) {
        when (result) {
             EDIT_EVENT_RESULT_OK -> showEventSavedConfirmationMessage("Event has been updated")
        }
    }

    private fun showEventSavedConfirmationMessage(text: String) = viewModelScope.launch {
        eventsEventChannel.send(EventsEvent.ShowEventSavedConfirmationMessage(text))
    }

    fun onDeleteAllCompletedClick() = viewModelScope.launch {
        eventsEventChannel.send(EventsEvent.NavigateToDeleteAllCompletedScreen)
    }

    sealed class EventsEvent {
        data class NavigateToEditEventScreen(val event: Event) : EventsEvent()
        data class ShowUndoDeleteEventMessage(val event: Event) : EventsEvent()
        data class ShowEventSavedConfirmationMessage(val msg: String) : EventsEvent()
        object NavigateToDeleteAllCompletedScreen : EventsEvent()
    }
}