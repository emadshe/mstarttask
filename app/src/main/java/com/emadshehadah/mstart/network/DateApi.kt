package com.emadshehadah.mstart.network

import com.emadshehadah.mstart.data.BaseWrapper
import com.emadshehadah.mstart.data.DateResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface DateApi {
    companion object {
        const val CONVERT_END_POINT = "gToH"
    }

    @GET(CONVERT_END_POINT)
    suspend fun convertDate(
        @Query("date") date: String,
    ): BaseWrapper<DateResponse>
}