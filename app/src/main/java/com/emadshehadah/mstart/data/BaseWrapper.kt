package com.emadshehadah.mstart.data


import com.google.gson.annotations.SerializedName

data class BaseWrapper<T>(
    @SerializedName("code")
    var code: Int = 0,
    @SerializedName("data")
    var data: T? = null,
    @SerializedName("status")
    var status: String = ""
)