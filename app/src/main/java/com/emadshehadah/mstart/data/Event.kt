package com.emadshehadah.mstart.data

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "event_table")
@Parcelize
data class Event(
    val name: String,
    val important: Boolean = false,
    val completed: Boolean = false,
    val created: String = "",
    @PrimaryKey(autoGenerate = true) val id: Int = 0
) : Parcelable