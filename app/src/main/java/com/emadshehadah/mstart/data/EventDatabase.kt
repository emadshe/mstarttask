package com.emadshehadah.mstart.data

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.emadshehadah.mstart.di.ApplicationScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Provider

@Database(entities = [Event::class], version = 1)
abstract class EventDatabase : RoomDatabase() {

    abstract fun eventDao(): EventDao

    class Callback @Inject constructor(
        private val database: Provider<EventDatabase>,
        @ApplicationScope private val applicationScope: CoroutineScope
    ) : RoomDatabase.Callback() {

        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)

            val dao = database.get().eventDao()

            applicationScope.launch {
                dao.insert(Event("Wash Car" , created =  "26-04-1442"))
                dao.insert(Event("Do the laundry", created =  "09-06-1442"))
                dao.insert(Event("Study in my project", important = true, created =  "14-02-1442"))
                dao.insert(Event("start development", completed = true, created =  "26-05-1442"))
                dao.insert(Event("Call mom", created =  "12-11-1442"))
            }
        }
    }
}