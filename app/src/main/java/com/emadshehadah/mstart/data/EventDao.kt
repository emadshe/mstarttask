package com.emadshehadah.mstart.data

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface EventDao {

    fun getEvents(query: String, sortOrder: SortOrder, hideCompleted: Boolean): Flow<List<Event>> =
        when (sortOrder) {
            SortOrder.BY_DATE -> getEventsSortedByDateCreated(query, hideCompleted)
            SortOrder.BY_NAME -> getEventsSortedByName(query, hideCompleted)
        }

    @Query("SELECT * FROM event_table WHERE (completed != :hideCompleted OR completed = 0) AND name LIKE '%' || :searchQuery || '%' ORDER BY important DESC, name")
    fun getEventsSortedByName(searchQuery: String, hideCompleted: Boolean): Flow<List<Event>>

    @Query("SELECT * FROM event_table WHERE (completed != :hideCompleted OR completed = 0) AND name LIKE '%' || :searchQuery || '%' ORDER BY important DESC, created")
    fun getEventsSortedByDateCreated(searchQuery: String, hideCompleted: Boolean): Flow<List<Event>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(event: Event)

    @Update
    suspend fun update(event: Event)

    @Delete
    suspend fun delete(event: Event)

    @Query("DELETE FROM event_table WHERE completed = 1")
    suspend fun deleteCompletedEvents()
}