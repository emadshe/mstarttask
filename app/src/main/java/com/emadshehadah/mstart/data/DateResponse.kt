package com.emadshehadah.mstart.data

import com.google.gson.annotations.SerializedName


data class DateResponse(
    @SerializedName("gregorian")
    var gregorian: Gregorian = Gregorian(),
    @SerializedName("hijri")
    var hijri: Hijri = Hijri()
) {
    data class Gregorian(
        @SerializedName("date")
        var date: String = "",
        @SerializedName("day")
        var day: String = "",
        @SerializedName("designation")
        var designation: Designation = Designation(),
        @SerializedName("format")
        var format: String = "",
        @SerializedName("month")
        var month: Month = Month(),
        @SerializedName("weekday")
        var weekday: Weekday = Weekday(),
        @SerializedName("year")
        var year: String = ""
    ) {
        data class Designation(
            @SerializedName("abbreviated")
            var abbreviated: String = "",
            @SerializedName("expanded")
            var expanded: String = ""
        )

        data class Month(
            @SerializedName("en")
            var en: String = "",
            @SerializedName("number")
            var number: Int = 0
        )

        data class Weekday(
            @SerializedName("en")
            var en: String = ""
        )
    }

    data class Hijri(
        @SerializedName("date")
        var date: String = "",
        @SerializedName("day")
        var day: String = "",
        @SerializedName("designation")
        var designation: Designation = Designation(),
        @SerializedName("format")
        var format: String = "",
        @SerializedName("holidays")
        var holidays: List<Any> = listOf(),
        @SerializedName("month")
        var month: Month = Month(),
        @SerializedName("weekday")
        var weekday: Weekday = Weekday(),
        @SerializedName("year")
        var year: String = ""
    ) {
        data class Designation(
            @SerializedName("abbreviated")
            var abbreviated: String = "",
            @SerializedName("expanded")
            var expanded: String = ""
        )

        data class Month(
            @SerializedName("ar")
            var ar: String = "",
            @SerializedName("en")
            var en: String = "",
            @SerializedName("number")
            var number: Int = 0
        )

        data class Weekday(
            @SerializedName("ar")
            var ar: String = "",
            @SerializedName("en")
            var en: String = ""
        )
    }
}

